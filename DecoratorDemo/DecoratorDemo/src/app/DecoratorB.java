package app;

public class DecoratorB extends Decorator {
    public void Operation(){
        super.Operation();;
        AddBehavior();
        System.out.println("Operation from Decorator B");
    }

    private void AddBehavior(){
        System.out.println("Decorator B Behavior");
    }

}