package app;

public abstract class Decorator extends Component {
    protected Component c;

    public void SetComponent(Component component){
        this.c = component;
    }

    public void Operation()
    {
        if(c != null)
        c.Operation();
    }
}
