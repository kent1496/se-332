package app;

public class DecoratorA extends Decorator {
    public void Operation(){
        super.Operation();
        System.out.print("Added Operation by DecoratorA");
    }

}