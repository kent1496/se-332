

import app.Component1;
import app.DecoratorA;
import app.DecoratorB;

public class App {

    public static void main(String[] args) throws Exception {
        System.out.println("Decorator");
        Component1 c = new Component1();
        DecoratorA a = new DecoratorA();
        DecoratorB b = new DecoratorB();
      
        a.SetComponent(c);
        a.Operation();
        b.SetComponent(c);
        b.Operation();
        a.SetComponent(b);
        a.Operation();

        
    }
}