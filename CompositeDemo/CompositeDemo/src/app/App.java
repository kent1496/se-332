package app;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Composite Demo");

        Composite root = new Composite("root");
        root.Add(new Leaf("Leaf A"));
        root.Add(new Leaf("Leaf B"));
 
        Composite comp = new Composite("Composite X");
        comp.Add(new Leaf("Leaf XA"));
        comp.Add(new Leaf("Leaf XB"));
 
        Composite comp2 = new Composite("Composite X2");
        comp2.Add(new Leaf("Leaf X2A"));
        comp2.Add(new Leaf("Leaf X2B"));
        comp.Add(comp2);
        root.Add(comp);
        //root.Add(comp2);
        root.Add(new Leaf("Leaf C"));

      // Add and remove a leaf
      Leaf leaf = new Leaf("Leaf D");
      root.Add(leaf);
      root.Remove(leaf);
      // Recursively display tree
      root.Display(1);
    }
}