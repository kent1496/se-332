package app;

public abstract class Component {
    protected String name;

    public Component(String n){
        name = n;
    }

    public abstract void Add(Component c);
    public abstract void Remove(Component c);
    public abstract void Display(int depth);
}