package app;

import java.util.ArrayList;
import java.util.Collections;

public class Composite extends Component {

    private ArrayList<Component> _children = new ArrayList<Component>();
    
    public Composite(String n){
        super(n);
    }  
    @Override
    public void Add(Component c) {
        _children.add(c);
    }

    @Override
    public void Remove(Component c) {
        _children.remove(c);
    }
    @Override
    public void Display(int depth) {
        
        System.out.println(String.join("", Collections.nCopies(depth, "-")) + name);
        for(Component c : _children)
        {
            c.Display(depth + 2);
        }
    }

}