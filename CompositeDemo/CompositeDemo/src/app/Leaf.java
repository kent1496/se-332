package app;

import java.util.Collections;

public class Leaf extends Component {

    public Leaf(String n)
    {
        super(n);
    }
    @Override
    public void Add(Component c) {
        System.out.println("Cannot add to a Leaf");
    }

    @Override
    public void Remove(Component c) {
        System.out.println("Cannot remove from a Leaf");
    }

    @Override
    public void Display(int depth) {
        System.out.println(String.join("", Collections.nCopies(depth, "-")) + name);
        

    }

}