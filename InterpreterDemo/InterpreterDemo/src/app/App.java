package app;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Interpreter Demo");
        String roman = "MCMXXVIII";
        Context context  = new Context(roman);

        List<Expression> tree = new ArrayList<Expression>();
        tree.add(new ThousandExpression());      
        tree.add(new HundredExpression());
        tree.add(new TenExpression());
        tree.add(new OneExpression());

        for(int i = 0;i<roman.length();i++){
            for(Expression e : tree){
                e.Interpret(context);
            }
        }
        System.out.println(roman + " = " + context.getIoutput());
    }
}