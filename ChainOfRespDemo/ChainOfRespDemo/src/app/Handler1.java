package app;

public class Handler1 extends Handler {

    @Override
    public void HandleRequest(int request) {
        if(request >= 0 && request < 10){
            String classname = this.getClass().getName();
            System.out.println(classname + " handled request "+ request);
        }
        else if (originalObj != null){
            originalObj.HandleRequest(request);
        }

    }

}