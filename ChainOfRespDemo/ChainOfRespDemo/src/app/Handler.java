package app;

public abstract class Handler {
    protected Handler originalObj;

    public void SetOriginator(Handler o){
        this.originalObj = o;
    }

    public abstract void HandleRequest(int request);
}