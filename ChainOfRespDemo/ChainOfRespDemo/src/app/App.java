package app;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Demo Chain of Responsibility");

        Handler h1 = new Handler1();
        Handler h2 = new Handler2();
        Handler h3 = new Handler3();
        h1.SetOriginator(h3);
        h2.SetOriginator(h2);

        int[] requests = {2, 6, 13, 25, 19, 4 ,28, 21};
        for(int r : requests)
        {
            h1.HandleRequest(r);
        }
    }
}