package app;

public class Handler2 extends Handler{

    @Override
    public void HandleRequest(int request) {
        if (request >= 10 && request < 20){
            String classname = this.getClass().getName();
            System.out.println(classname + " handled request "+ request);
        }
        else if (originalObj != null){
            originalObj.HandleRequest(request);
        }
    }

}