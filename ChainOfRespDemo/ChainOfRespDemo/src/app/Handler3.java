package app;

public class Handler3 extends Handler{

    @Override
    public void HandleRequest(int request) {
        if (request >= 20 && request < 30){
            String classname = this.getClass().getName();
            System.out.println(classname + " handled request "+ request);
        }
        else if (originalObj != null){
            originalObj.HandleRequest(request);
        }
    }

}