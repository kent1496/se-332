package app;

public class Invoker{
    private Command c;

    public void SetCommand(Command command){
        this.c = command;
    }

    public void ExecuteCommand()
    {
        c.Excute();
    }
}