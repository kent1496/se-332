package app;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Command Demo");
        Receiver r = new Receiver();
        Command c = new Command1(r);
        Invoker invoker = new Invoker();

        invoker.SetCommand(c);
        invoker.ExecuteCommand();
    }
}