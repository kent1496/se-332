package app;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Iterator Demo");

        NameRepository nameRepository = new NameRepository();

        for(Iterator iter = nameRepository.getIterator(); iter.hasNext();){
            String name = (String)iter.Next();
            System.out.println("Name : " + name);
        }
    }
}