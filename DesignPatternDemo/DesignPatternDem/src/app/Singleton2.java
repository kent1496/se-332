package app;

public class Singleton2 {
    private static Singleton2 instance = new Singleton2();

    private Singleton2(){}

    public static Singleton2 getInstance(){
        return instance;
    }

    private int simplevalue;

    public void SetInt(int i){this.simplevalue = i;}

    public void ShowMessage(){
        System.out.println("Print from singleton" + simplevalue);
    }
}