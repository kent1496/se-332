package app;


import java.util.ArrayList;

public class Product {
    private ArrayList<String> _parts = new ArrayList<String>();

    public void Add(String part)
    {
        _parts.add(part);
    }

    public void Show(){
        System.out.println("\nProduct Parts ------");
        for(String part :  _parts){
            System.out.println(part);
        }
    }
}