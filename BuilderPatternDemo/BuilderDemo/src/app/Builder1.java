package app;

public class Builder1 extends Builder {
    private Product _product = new Product();

	@Override
	public void BuildPartA() {
		_product.Add("Part A");
	}

    @Override
    public void BuildPartB() {
        _product.Add("Part B");
    }

    @Override
    public Product GetResult() {
        return _product;
    }
}