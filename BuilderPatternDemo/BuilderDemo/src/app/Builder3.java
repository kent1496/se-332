package app;

public class Builder3 extends Builder {
    private Product _product = new Product();

	@Override
	public void BuildPartA() {
		_product.Add("Part C");
	}

    @Override
    public void BuildPartB() {
        _product.Add("PartD");
    }

    @Override
    public Product GetResult() {
        return _product;
    }
}