package app;

public class Builder2 extends Builder {
    private Product _product = new Product();

	@Override
	public void BuildPartA() {
		_product.Add("Part X");
	}

    @Override
    public void BuildPartB() {
        _product.Add("Part Y");
    }

    @Override
    public Product GetResult() {
        return _product;
    }
}