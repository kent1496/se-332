package app;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello Java");

        Product pp = new Product();

        Director director = new Director();

        Builder b1 = new Builder1();
        Builder b2 = new Builder2();

        director.Construct(b1);
        Product p1 = b1.GetResult();
        p1.Show();

        director.Construct(b2);
        Product p2 = b2.GetResult();
        p2.Show();
        
        Builder b3 = new Builder3();
        director.Construct(b3);
        Product p3 = b3.GetResult();
        p3.Show();
    }
}