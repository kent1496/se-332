package app;

public class ImplementedSubject extends AbstractSubject {

    @Override
    public void Request() {
        System.out.println("The method Request from ImplementedSubject is called");
    }

}