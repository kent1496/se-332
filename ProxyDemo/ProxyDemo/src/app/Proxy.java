package app;

public class Proxy extends AbstractSubject{

private ImplementedSubject impSubj;

@Override
	public void Request() {
        if(impSubj == null){
            impSubj = new ImplementedSubject();
        }

        impSubj.Request();
	}

}