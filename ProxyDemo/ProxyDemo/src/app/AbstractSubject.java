package app;

public abstract class AbstractSubject {
    public abstract void Request();
}