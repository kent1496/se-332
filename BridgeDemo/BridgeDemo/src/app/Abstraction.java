package app;

public class Abstraction {
    protected Implementor im;

    public void SetImplementor(Implementor i){
        this.im = i;
    }

    public void Operation(){
        im.Operation();
    }
}