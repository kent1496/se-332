package app;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Bridge Demo");
        Abstraction ab = new RefinedAbstraction();

        ab.SetImplementor(new ImplementorA());
        ab.Operation();
        ab.SetImplementor(new ImplementorB());
        ab.Operation();
    }
}